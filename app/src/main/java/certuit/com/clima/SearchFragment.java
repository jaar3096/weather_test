package certuit.com.clima;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import certuit.com.clima.model.City;
import certuit.com.clima.model.CityJSON;
import certuit.com.clima.model.Weather;
import certuit.com.clima.model.WeatherJSON;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    // The SearchFragment does almost the same thing as the MainFragment, with some minor differences

    private RecyclerView recyclerView;
    private final static String URL = "http://api.openweathermap.org/data/2.5/forecast?APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";
    private Button button;
    private EditText editText;
    private RadioGroup radioGroupCity;
    private RadioButton radioButtonCity;
    private RadioGroup radioGroupCP;
    private RadioButton radioButtonCP;
    private TextView city_name;

    private OnFragmentInteractionListener mListener;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        city_name = view.findViewById(R.id.text_city_name_get_search);
        button = view.findViewById(R.id.button_search);
        editText = view.findViewById(R.id.editText_search);
        radioGroupCity = view.findViewById(R.id.radio_group_city);
        radioGroupCP = view.findViewById(R.id.radio_group_cp);
        radioButtonCity = view.findViewById(R.id.radio_city);
        radioButtonCP = view.findViewById(R.id.radio_cp);
        recyclerView = view.findViewById(R.id.recyclerViewSearch);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        radioButtonCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioGroupCP.clearCheck(); //clear the Zipcode radio group
            }
        });

        radioButtonCP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioGroupCity.clearCheck(); //clear the city radio group
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlFinal = "";
                // Depending on what radio button is checked, the URL will change
                if (radioButtonCity.isChecked()){
                    // search by city's name
                    urlFinal = "&q=";
                }else if (radioButtonCP.isChecked()){
                    urlFinal = "&zip=";
                    // search by zipcode
                }else if (!radioButtonCP.isChecked() && !radioButtonCity.isChecked()){
                    Toast.makeText(getContext(), "Por favor selecciona una de las opciones",
                            Toast.LENGTH_LONG).show();
                }
                if (!urlFinal.isEmpty()){
                    RequestQueue queue = Volley.newRequestQueue(getContext());
                    urlFinal = URL + urlFinal + editText.getText().toString();
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, urlFinal, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try{
                                City ciudad = CityJSON.getForecastCity(new JSONObject(response));
                                ciudad.setForecast(WeatherJSON.getForecast(new JSONObject(response)));
                                if (ciudad.getForecast() != null){
                                    CityForecastAdapter adapter = new CityForecastAdapter(ciudad,
                                            CityForecastAdapter.CITY_ADAPTER);
                                    recyclerView.setAdapter(adapter);
                                    city_name.setText(ciudad.getName());
                                }else{
                                    Toast.makeText(getContext(), "Ciudad no encontrada",
                                            Toast.LENGTH_LONG).show();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(), "Verifica tu conexión a Internet",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                    queue.add(stringRequest);
                }
            }
        });

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

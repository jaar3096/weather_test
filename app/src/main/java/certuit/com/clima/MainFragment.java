package certuit.com.clima;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import certuit.com.clima.model.City;
import certuit.com.clima.model.CityJSON;
import certuit.com.clima.model.WeatherJSON;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    private RecyclerView recyclerView; //RecyclerView used to display the information
    // URL for the GET request
    private final static String URL = "http://api.openweathermap.org/data/2.5/forecast?q=mexicali&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";

    private OnFragmentInteractionListener mListener; //unused interface

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        // Links the recyclerview to the one in the layout
        recyclerView = view.findViewById(R.id.recyclerViewMexicali);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        /*
         * Volley library to manages the GET request, the queue used manages all the added requests
         */
        RequestQueue queue = Volley.newRequestQueue(this.getContext());
        //StringRequest used to make the prepare the GET request
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // Getting the city's and its weather info through the static method
                            City mexicali = CityJSON.getForecastCity(new JSONObject(response));
                            mexicali.setForecast(WeatherJSON.getForecast(new JSONObject(response)));
                            if (mexicali.getForecast() != null) {
                                // Sets the adapter to show the info
                                CityForecastAdapter adapter = new CityForecastAdapter(mexicali,
                                        CityForecastAdapter.MEXICALI_ADAPTER);
                                recyclerView.setAdapter(adapter);
                            }else {
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // No internet connection or the city was not found
                Toast.makeText(getContext(), "Verifique su conexión a Internet", Toast.LENGTH_LONG).show();
            }
        });
        //added the string request to the queue
        queue.add(stringRequest);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

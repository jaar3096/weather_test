package certuit.com.clima;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import certuit.com.clima.model.City;

/**
 * Created by Jorge Aguirre on 06/02/2018.
 * This class will be used as an adapter of a recycler view, this particular adapter will work for
 * the MainFragment and favoriteFragment, because those fragments will make the GET request of the
 * current weather, so the RecyclerView will display an ArrayList of Weathe
 *
 */

public class CityWeatherAdapter extends RecyclerView.Adapter<ViewHolderCityWeather>{

    private ArrayList<City> cities; //Array that will be used to display the 5 cities

    /**
     * Constructor that receives the full ArrayList<City>
     * @param cities
     */
    public CityWeatherAdapter(ArrayList<City> cities){
        this.cities = cities;
    }

    /**
     * Method that inflates the favorites fragment layout
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolderCityWeather onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_favorites, null, false);
        return new ViewHolderCityWeather(view);
    }

    /**
     * Method that passes the information to the ViewHolder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolderCityWeather holder, int position) {
        holder.assignateData(cities.get(position));
    }

    /**
     * Method that gets the ArrayList size
     * @return
     */
    @Override
    public int getItemCount() {
        return cities.size();
    }
}

package certuit.com.clima;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import certuit.com.clima.model.City;
import certuit.com.clima.model.CityJSON;
import certuit.com.clima.model.WeatherJSON;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavoriteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView recyclerView;
    private final static String URL_SAN_DIEGO = "http://api.openweathermap.org/data/2.5/weather?q=San Diego&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";
    private final static String URL_LONDON = "http://api.openweathermap.org/data/2.5/weather?q=london&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";
    private final static String URL_MADRID = "http://api.openweathermap.org/data/2.5/weather?q=Madrid&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";
    private final static String URL_PARIS = "http://api.openweathermap.org/data/2.5/weather?q=Paris&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";
    private final static String URL_MONTREAL = "http://api.openweathermap.org/data/2.5/weather?q=Montreal&APPID=2ecceca5ed91b00208d59e0cf25e0d33&units=Metric";

    public FavoriteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoriteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoriteFragment newInstance(String param1, String param2) {
        return new FavoriteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewFavorite);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        final ArrayList<City> cities = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest requestSanDiego = new StringRequest(Request.Method.GET, URL_SAN_DIEGO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            City sanDiego = CityJSON.getCurrentCity(new JSONObject(response));
                            sanDiego.setWeather(WeatherJSON.getCurrentWeather(new JSONObject(response)));
                            if (sanDiego.getWeather() != null){
                                cities.add(sanDiego);
                                CityWeatherAdapter adapter = new CityWeatherAdapter(cities);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ciudad no encontrada",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Verifique su conexión a Internet",
                        Toast.LENGTH_LONG).show();
            }
        });
        queue.add(requestSanDiego);
        StringRequest requestLondon = new StringRequest(Request.Method.GET, URL_LONDON,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            City london = CityJSON.getCurrentCity(new JSONObject(response));
                            london.setWeather(WeatherJSON.getCurrentWeather(new JSONObject(response)));
                            if (london.getWeather() != null){
                                cities.add(london);
                                CityWeatherAdapter adapter = new CityWeatherAdapter(cities);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ciudad no encontrada",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Verifique su conexión a Internet",
                        Toast.LENGTH_LONG).show();
            }
        });
        queue.add(requestLondon);
        StringRequest requestParis = new StringRequest(Request.Method.GET, URL_PARIS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            City paris = CityJSON.getCurrentCity(new JSONObject(response));
                            paris.setWeather(WeatherJSON.getCurrentWeather(new JSONObject(response)));
                            if (paris.getWeather() != null){
                                cities.add(paris);
                                CityWeatherAdapter adapter = new CityWeatherAdapter(cities);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ciudad no encontrada",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Verifique su conexión a Internet",
                        Toast.LENGTH_LONG).show();
            }
        });
        queue.add(requestParis);
        StringRequest requestMadrid = new StringRequest(Request.Method.GET, URL_MADRID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            City madrid = CityJSON.getCurrentCity(new JSONObject(response));
                            madrid.setWeather(WeatherJSON.getCurrentWeather(new JSONObject(response)));
                            if (madrid.getWeather() != null){
                                cities.add(madrid);
                                CityWeatherAdapter adapter = new CityWeatherAdapter(cities);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ciudad no encontrada",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Verifique su conexión a Internet",
                        Toast.LENGTH_LONG).show();
            }
        });
        queue.add(requestMadrid);
        StringRequest requestMontreal = new StringRequest(Request.Method.GET, URL_MONTREAL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            City montreal = CityJSON.getCurrentCity(new JSONObject(response));
                            montreal.setWeather(WeatherJSON.getCurrentWeather(new JSONObject(response)));
                            if (montreal.getWeather() != null){
                                cities.add(montreal);
                                CityWeatherAdapter adapter = new CityWeatherAdapter(cities);
                                recyclerView.setAdapter(adapter);
                            }else{
                                Toast.makeText(getContext(), "Algo no funciono", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Ciudad no encontrada",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Verifique su conexión a Internet",
                        Toast.LENGTH_LONG).show();
            }
        });
        queue.add(requestMontreal);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

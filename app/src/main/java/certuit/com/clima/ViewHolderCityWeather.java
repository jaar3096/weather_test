package certuit.com.clima;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import certuit.com.clima.model.City;

/**
 * Created by Jorge Aguirre on 06/02/2018.
 * This class serves as a ViewHolder for the RecyclerView, the TextViews are linked a specific
 * layout.
 */

public class ViewHolderCityWeather extends RecyclerView.ViewHolder {

    //TextViews where the info will be displayed
    private TextView temperature;
    private TextView temp_max;
    private TextView temp_min;
    private TextView humidity;
    private TextView city_name;

    /**
     * Method that links the TextViews to the specific layout
     * @param itemView Inflated view from the layout
     */
    public ViewHolderCityWeather(View itemView) {
        super(itemView);
        temperature = itemView.findViewById(R.id.text_temp_get_favorites);
        temp_max = itemView.findViewById(R.id.text_tem_max_get_favorites);
        temp_min = itemView.findViewById(R.id.text_temp_min_get_favorites);
        humidity = itemView.findViewById(R.id.text_humidity_get_favorites);
        city_name = itemView.findViewById(R.id.text_city_name_get_favorites);
    }

    /**
     * Method that displays the City info and its Weather info on the TextViews.
     * @param city City used to display its info.
     */
    public void assignateData(City city){
        temperature.setText("Temperatura: " + city.getWeather().getTempterature() + "°C");
        temp_max.setText("Temperatura Maxima: " + city.getWeather().getTemp_max() + "°C");
        temp_min.setText("Temperatura Minima: " + city.getWeather().getTemp_min() + "°C");
        humidity.setText("Humedad: " + city.getWeather().getHumidity() + "%");
        city_name.setText("Ciudad: " + city.getName());
    }
}

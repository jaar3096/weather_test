package certuit.com.clima;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import certuit.com.clima.R;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout; //DrawerMenu
    private ActionBarDrawerToggle toggle; //Hamburger Icon

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme); //splash screen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = findViewById(R.id.main_activity);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open,
                R.string.close);
        drawerLayout.addDrawerListener(toggle);
        NavigationView navigationView = findViewById(R.id.nv);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupDrawerContent(navigationView);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content, new MainFragment()).commit();
        navigationView.setCheckedItem(R.id.main_screen);
    }

    /**
     * Method that selects what fragment is displayed, depending on the selection of the drawer menu
     * @param menuItem Item selected of the drawer menu
     */
    public void selectItemDrawer(MenuItem menuItem){
        Fragment fragment = null;
        Class fragmentClass;
        /* The fragment class is selected through this switch case, then it makes a new instance of
         * that fragment.
         */
        switch (menuItem.getItemId()){
            case R.id.main_screen:
                fragmentClass = MainFragment.class;
                break;
            case R.id.search_screen:
                fragmentClass = SearchFragment.class;
                break;
            case R.id.favorite_screen:
                fragmentClass = FavoriteFragment.class;
                break;
            default:
                fragmentClass = MainFragment.class;
        }
        // here the new instances is created
        try{
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        //then with the fragment manager the layout it's replaced
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content, fragment).commit();
        //makes that the selected item of the drawer is selected.
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();
    }

    private void setupDrawerContent (NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return toggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}

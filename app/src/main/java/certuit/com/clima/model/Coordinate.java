package certuit.com.clima.model;

/**
 * Created by Jorge Aguirre on 31/01/2018.
 * This class defines a coordinate, used in the City class.
 */

public class Coordinate {

    private double longitude;
    private double latitude;

    /**
     *
     * @param longitude longitude attribute
     * @param latitude latitude attribute
     */
    public Coordinate(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     *
     */
    public Coordinate(){}

    /**
     *
     * @return Returns the longitude stored in the Coordinate Object
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude New longitude will be used
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return Returns the latitude stored in the Coordinate Object
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude New latitude will be used
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return The info of the coordinate
     */
    @Override
    public String toString() {
        return "Coordinate{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}

package certuit.com.clima.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jorge Aguirre on 31/01/2018.
 * This class defines the weather object, this object will be used to manage the weather's information
 * including, temperature, minimim temperature, maximum temperature, humidity, date and time.
 */

public class Weather {

    private String tempterature;
    private String temp_max;
    private String temp_min;
    private String humidity;
    private String date_time;

    /**
     * Constructor that recieves all the city's attributes
     * @param tempterature Current temperature
     * @param temp_max Maximum temperature
     * @param temp_min Minumim temperature
     * @param humidity Humidity of the city
     * @param date_time Date and time (only in the 5 day forecast)
     */
    public Weather(String tempterature, String temp_max, String temp_min,
            String humidity, String date_time) {
        this.tempterature = tempterature;
        this.temp_max = temp_max;
        this.temp_min = temp_min;
        this.humidity = humidity;
        this.date_time = date_time;
    }

    /**
     * Empty constructor
     */
    public Weather(){}

    private Weather(Parcel in) {

        String[] data = new String[5];
        in.readStringArray(data);
        tempterature = in.readString();
        temp_max = in.readString();
        temp_min = in.readString();
        humidity = in.readString();
        date_time = in.readString();
    }

    /**
     *
     * @return The current temperature stored in the weather object
     */
    public String getTempterature() {
        return tempterature;
    }

    /**
     *
     * @param tempterature Updates the current temperature
     */
    public void setTempterature(String tempterature) {
        this.tempterature = tempterature;
    }

    /**
     *
     * @return The maximum temperature stored in the weather object
     */
    public String getTemp_max() {
        return temp_max;
    }

    /**
     *
     * @param temp_max Updates the stored maximum temperature
     */
    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }

    /**
     *
     * @return The minimum temperature stored in the weather object
     */
    public String getTemp_min() {
        return temp_min;
    }

    /**
     *
     * @param temp_min Updates the stored minimum temperature
     */
    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    /**
     *
     * @return The humidity percentage of the city
     */
    public String getHumidity() {
        return humidity;
    }

    /**
     *
     * @param humidity Updates the stored value for the humidity percentage
     */
    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    /**
     *
     * @return The date and time (forecast only)
     */
    public String getDate_time() {
        return date_time;
    }

    /**
     *
     * @param date_time updates the date and time stored
     */
    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    /**
     *
     * @return Information of the weather object
     */
    @Override
    public String toString() {
        return "Weather{" +
                "tempterature='" + tempterature + '\'' +
                ", temp_max='" + temp_max + '\'' +
                ", temp_min='" + temp_min + '\'' +
                ", humidity='" + humidity + '\'' +
                ", date_time='" + date_time + '\'' +
                '}';
    }

}

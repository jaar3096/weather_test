package certuit.com.clima.model;

import java.util.ArrayList;

/**
 * Created by Jorge Aguirre on 31/01/2018.
 * This class defines the object of a city, this object will be used to manage a city's information
 * including, name, id, country, weather, forecast and a coordinate.
 */

public class City {

    private int id;
    private String name;
    private String country;
    private Coordinate coordinate;
    private Weather weather;
    private ArrayList<Weather> forecast;

    /**
     *
     * @param id ID of the city
     * @param name Name of the city
     * @param country Coutry the city belongs to.
     * @param coordinate Coordinate of the city
     * @param weather Current Weather in the city
     * @param forecast 5 day forecast of the city
     *
     * Constructor that recieves all the city's attributes
     */
    public City(int id, String name, String country, Coordinate coordinate, Weather weather, ArrayList<Weather> forecast) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.coordinate = coordinate;
        this.weather = weather;
        this.forecast = forecast;
    }

    /**
     * Empty constructor
     */
    public City(){}

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     *
     * @param coordinate
     */
    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     *
     * @return
     */
    public Weather getWeather() {
        return weather;
    }

    /**
     *
     * @param weather
     */
    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    /**
     *
     * @return
     */
    public ArrayList<Weather> getForecast() {
        return forecast;
    }

    /**
     *
     * @param forecast
     */
    public void setForecast(ArrayList<Weather> forecast) {
        this.forecast = forecast;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", coordinate=" + coordinate +
                ", weather=" + weather +
                ", forecast=" + forecast +
                '}';
    }
}

package certuit.com.clima.model;

import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jorge Aguirre on 03/02/2018.
 * This class will be used to manage the response of the GET request to the openweather api, it will
 * extract the info of the JSON Object and fill City objects.
 */

public class CityJSON {


    /**
     * Constants to read the JSON object
     */
    private static final String _ID = "id";
    private static final String NAME = "name";
    private static final String COUNTRY = "country";
    private static final String COORD = "coord";
    private static final String LON = "lon";
    private static final String LAT = "lat";


    /**
     * This method will extract the city info from the current weather request.
     * @param response
     * @return
     */
    public static City getCurrentCity(JSONObject response) {
        City city = new City();
        try {
            // Reading the JSON response
            city.setId(response.getInt(_ID));
            city.setName(response.getString(NAME));
            JSONObject coord = response.getJSONObject(COORD);
            city.setCoordinate(new Coordinate(coord.getDouble(LON),
                    coord.getDouble(LAT)));
            JSONObject country = response.getJSONObject("sys");
            city.setCountry(country.getString(COUNTRY));
        } catch (JSONException e) {
            e.printStackTrace();
            city = null;
        }
        return city;
    }

    /**
     * This method will extract the city info from the forecast request.
     * @param response
     * @return
     */
    public static City getForecastCity(JSONObject response){
        City city = new City();
        try{
            //reading the JSON response
            JSONObject cityJSON = response.getJSONObject("city");
            //city.setId(cityJSON.getInt(_ID));
            city.setName(cityJSON.getString(NAME));
            city.setCountry(cityJSON.getString(COUNTRY));
            JSONObject coord = cityJSON.getJSONObject(COORD);
            city.setCoordinate(new Coordinate(coord.getDouble(LON),
                    coord.getDouble(LAT)));

        }catch (JSONException e){
            e.printStackTrace();
            city = null;
        }
        return city;
    }

}

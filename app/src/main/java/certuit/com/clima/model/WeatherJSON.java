package certuit.com.clima.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jorge Aguirre on 31/01/2018.
 * This class will be used to manage the response of the GET request to the openweather api, it will
 * extract the info of the JSON Object and fill Weather objects.
 */

public class WeatherJSON {

    /**
     * Constants to read the JSON object
     */
    private static final String TEMPERATURE = "temp";
    private static final String TEMP_MAX = "temp_max";
    private static final String TEMP_MIN = "temp_min";
    private static final String HUMIDITY = "humidity";
    private static final String DATE = "dt_txt";


    /**
     * This method will extract the weather info from the current weather request.
     * @param response
     * @return
     */
    public static Weather getCurrentWeather(JSONObject response){
        Weather weather = new Weather();
        try {
            JSONObject weatherJSON = response.getJSONObject("main");
            weather.setTempterature(String.valueOf(weatherJSON.getDouble(TEMPERATURE)));
            weather.setTemp_max(String.valueOf(weatherJSON.getDouble(TEMP_MAX)));
            weather.setTemp_min(String.valueOf(weatherJSON.getDouble(TEMP_MIN)));
            weather.setHumidity(String.valueOf(weatherJSON.getInt(HUMIDITY)));
        }catch (JSONException e) {
            e.printStackTrace();
            weather = null;
        }
        return weather;
    }

    /**
     * This method will extract the weather info from the forecast request.
     * @param response
     * @return
     */
    public static ArrayList<Weather> getForecast(JSONObject response){
        ArrayList<Weather> forecast = new ArrayList<>();
        try{
            JSONArray array = response.getJSONArray("list");
            int i;
            for (i = 0; i < array.length(); i++){
                JSONObject weather = array.getJSONObject(i);
                forecast.add(new Weather());
                forecast.get(i).setDate_time(weather.getString(DATE));
                weather = weather.getJSONObject("main");
                forecast.get(i).setTempterature(String.valueOf(weather.getDouble(TEMPERATURE)));
                forecast.get(i).setTemp_max(String.valueOf(weather.getDouble(TEMP_MAX)));
                forecast.get(i).setTemp_min(String.valueOf(weather.getDouble(TEMP_MIN)));
                forecast.get(i).setHumidity(String.valueOf(weather.getInt(HUMIDITY)));
            }
        }catch (JSONException e){
            e.printStackTrace();
            forecast = null;
        }
        return forecast;
    }
}

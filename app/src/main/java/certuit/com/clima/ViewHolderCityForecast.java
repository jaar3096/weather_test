package certuit.com.clima;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import certuit.com.clima.model.Weather;

/**
 * Created by Jorge Aguirre on 01/02/2018.
 * This class serves as a ViewHolder for the RecyclerView, depending on what type of adapter the user
 * requires, it links to differents layouts.
 */

class ViewHolderCityForecast extends RecyclerView.ViewHolder {

    //TextViews where the info will be displayed
    private TextView temperature;
    private TextView temp_max;
    private TextView temp_min;
    private TextView humidity;
    private TextView date;
    private int adapter;

    /**
     * Method where depending on the type of adapter, the TextViews will be linked to different
     * View's ID.
     * @param itemView View of the layout to use
     * @param adapter type of layout to used
     */
    ViewHolderCityForecast(View itemView, int adapter) {
        super(itemView);
        this.adapter = adapter;

        switch (this.adapter){
            case 1:
                temperature = itemView.findViewById(R.id.text_temp_get);
                temp_max = itemView.findViewById(R.id.text_tem_max_get);
                temp_min = itemView.findViewById(R.id.text_temp_min_get);
                humidity = itemView.findViewById(R.id.text_humidity_get);
                date = itemView.findViewById(R.id.text_date_get);
                break;
            case 2:
                temperature = itemView.findViewById(R.id.text_temp_get_card);
                temp_max = itemView.findViewById(R.id.text_tem_max_get_card);
                temp_min = itemView.findViewById(R.id.text_temp_min_get_card);
                humidity = itemView.findViewById(R.id.text_humidity_get_card);
                date = itemView.findViewById(R.id.text_date_get_card);
                break;
            default:

                break;
        }
    }

    /**
     * Method that displays the Weather info on the TextViews
     * @param weather Weather Object that will be displayed in the List
     */
    void asignateData(Weather weather) {
        temperature.setText("Temperatura: " + weather.getTempterature() + "°C");
        temp_max.setText("Temperatura Maxima: " + weather.getTemp_max() + "°C");
        temp_min.setText("Temperatura Minima: " + weather.getTemp_min() + "°C");
        humidity.setText("Humedad: " + weather.getHumidity() + "%");
        date.setText("Fecha y Hora: " + weather.getDate_time());
    }
}

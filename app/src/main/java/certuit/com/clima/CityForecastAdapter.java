package certuit.com.clima;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import certuit.com.clima.model.City;

/**
 * Created by Jorge Aguirre on 01/02/2018.
 * This class will be used as an adapter of a recycler view, this particular adapter will work for
 * the MainFragment and favoriteFragment, because those fragments will make the GET request of the
 * 5 days forecast, so the RecyclerView will display an ArrayList of Weathers.
 */

public class CityForecastAdapter extends RecyclerView.Adapter<ViewHolderCityForecast>{

    private City city; //City that will be used to retrive its forecast
    private int adapter; // int for a switch case

    /**
     * Depending of what number is used, it will load a different View.
     */
    final static int MEXICALI_ADAPTER = 1;
    final static int CITY_ADAPTER = 2;


    /**
     * Constructor that receives a City Object and an integer.
     * @param city
     * @param adapter
     */
    public CityForecastAdapter(City city, int adapter) {
        this.adapter = adapter;
        this.city = city;
    }

    /**
     * In this method it will be decided which view will be inflated.
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolderCityForecast onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (adapter){
            case MEXICALI_ADAPTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_view_recycler, null, false);
                break;
            case CITY_ADAPTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_view_card, null, false);
                break;
            default:
                // I don't know
                break;
        }
        return new ViewHolderCityForecast(view, adapter);
    }

    /**
     * In this method the information is passed to the ViewHolder to be displayed.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolderCityForecast holder, int position) {
        holder.asignateData(city.getForecast().get(position));
    }

    /**
     * this method gets the size of the ArrayList
     * @return
     */
    @Override
    public int getItemCount() {
        return city.getForecast().size();
    }
}
